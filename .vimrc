" VIM Configuration - Cédrik Bernard
" 
" * Pas définitif

" Général

set nocompatible

set encoding=utf-8


" Options fenêtre

set title          "  Affiche le titre du fichier dans la fenêtre

set nu             "  Affiche le n° de ligne

"set relativenumber " *Affiche les n° de ligne de manière relative

set nowrap         " *Ne fait pas de retour à la ligne

set scrolloff=3    "  Le scroll vertical commence 3 lignes avant la fin de le fenêtre

set sidescrolloff=10
                   "  Le scroll horizontal commence 5 colonnes avant la fin de la fenêtre
set hidden         "  Ouvrir un fichier ne ferme par le fichier précedent

set ruler          "  Afficher le n° de colone dans vim (si lightline pas installé)

set wildmenu       "  Afficher le menu des commandes

set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx
                   "  Ignore les fichiers binaires des propositions
                   "  d'ouverture
"set cursorline    "  Afficher la ligne où le curseur se trouve

set mouse=a        "  Permet d'utiliser la souris pour scroller et changer la taille

set timeoutlen=1000 ttimeoutlen=10
                   "  Optimisation of esc
set showmatch      "  Affiche les couples de parenthèses

set showtabline=2  "  Affiche toujours la ligne des onglets/tabs, même si un seul onglet


" Syntaxe

filetype on        "  Détecte le type de fichier 

syntax on          "  Colorise la syntaxe


" Options de la barre de status

set showcmd        "  Affiche dans la barre de status les frappes dans le mode normal 

set statusline=%<  "  Partie à tronquer si pas de place
set statusline+=%f "  Chemin et nom du fichier
set statusline+=\ %h 
                   "  Flag help
set statusline+=%m "  Flag modifié
set statusline+=%r "  Flag lecture seule
set statusline+=%= "  Séparation entre gauche et droite
set statusline+=\[\ %{&filetype}\ \| 
                   "  Type de fichier (sh, html ..)
set statusline+=\ %{&fileformat}\  
                   "  Format de fichier (unix, ..)
set statusline+=%{\"\|\ \".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"\ \]\ \"}
                   "  Encodage du fichier (utf-8, ..)
set statusline+=%k\ 
                   "  Keymap
set statusline+=%l, 
                   "  Numero de ligne  
set statusline+=%c "  Numero de colonne
set statusline+=%V "  Numero de colonne virtuelle (s'affiche si différent de colonne)
set statusline+=%-14.(%)
                   "  14 espaces à droite
set statusline+=%P "  Pourcentage du 


" Options de recherche

set ignorecase     "  Ignore la casse pour la recherche

set smartcase      "  La casse redevient importante s'il y a une majuscule dans le terme

set hlsearch       "  Surbrillance les resultats

set wrapscan       "  Recherche cyclique


" Options navigation de dossier/fichier

set path+=**       "  La recherche de fichier est recursive

set wildmenu       "  Affiche les fichiers qui match


" Sons

set visualbell     "  Affiche le beep

set noerrorbells   "  Supprime le son du beep


" Indentation

set autoindent     "  Indente automatiquement 

set tabstop=4      "  La touche tab montre 8 espaces

set shiftwidth=4   "  La touche tab montre 8 espaces

if has("autocmd")
        filetype plugin indent on
endif              "  Indentation suivant le langage (Extension du fichier)

set expandtab      "  Remplace les tab par des espaces


" Autocompletion

set complete=.,w,b,u,t,i
                   "  Complete le fichier avec les fichiers tags

if has("autocmd") && exists("+omnifunc")
	autocmd Filetype *
		    \	if &omnifunc == "" |
		    \		setlocal omnifunc=syntaxcomplete#Complete |
		    \	endif
endif              "  Autocomplétion suivant le langage (Ext du fichier)


" Opérations différencielles

set diffopt+=algorithm:patience
                   "  Algo patience

" Copier/coller

set clipboard=unnamedplus



" Cache

if isdirectory('~/.cache/vim')
                   "  Parce que j'aime avoir tous les .swp dans le même dossier
    set backupdir=~/.cache/vim
                   "  Directory des fichiers backups
    set dir=~/.cache/vim
                   "  Directory des fichiers swp
endif

set history=1000   "  Taille historique des changements de fichiers


" Plugin

"set runtimepath+=~/.vim/pack
                   "  Détermine le dossier 
"call plug#begin('~/.vim/plugged')
                   "  Début de la gestion des plugins par Vim-Plug
"Plug 'itchyny/lightline.vim'
                   "  plugin lightline qui affiche une ligne de status
"Plug 'chriskempson/base16-vim'
                   "  Thème base16
"Plug 'kshenoy/vim-signature'
                   "  Affiche les marks dans la colonne des n° de lignes
"Plug 'frazrepo/vim-rainbow'
                   "  Colore les parenthèses en pair


"call plug#end()    "  Fin de la gestion des plugins par Vim-Plug

"colorscheme base16-default-dark
                   "  Theme base16 dark


" Thème

color zellner      "  Couleur dark si pas de thème

set background=dark
                   "  Explique à Vim que la couleur d'arrière plan est foncée
highlight Normal guifg=white guibg=gray18
                   "  Correction couleurs pour gvim
highlight LineNr ctermfg=darkgrey ctermbg=NONE
                   "  Couleur des n° de lignes
highlight CursorLineNr cterm=none ctermbg=235 ctermfg=Yellow gui=bold guifg=Yellow
                   "  
highlight CursorLine ctermbg=235
                   "  La couleur de ligne de curseur (gris)
highlight Search ctermbg=206 ctermfg=16
                   "  La couleur arrière plan des résultats de recherche (rose)
                   "  La couleur avant-plan des résultats de recherche (noir)
highlight Visual  guifg=#000000 guibg=#ffaf87 gui=none ctermbg=216 ctermfg=black
                   "  Couleur de la sélection dans le mode visuel (orange saumon) 
highlight PmenuSel ctermbg=203 guibg=IndianRed1 ctermfg=white guifg=white

highlight Pmenu ctermbg=235 guibg=grey ctermfg=darkgray guifg=darkgray


" Patch pour lightline

set laststatus=2

if !has('gui_running')
  set t_Co=256
endif

set noshowmode     "  Cache le mode

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }          "  Thème de Lightline


" Mapage des touches:

nnoremap o o<Esc>
                   "  Revenir au mode normal après une nouvelle ligne après la ligne actuelle
nnoremap O O<Esc>
                   "  Revenir au mode normal après une nouvelle ligne à la ligne actuelle
inoremap jj <esc>gg<c-v>G$
nnoremap jj <esc>gg<c-v>G$
                   "  Enregister en mode insert avec 2xj
"inoremap <Up> <esc><Up>
"inoremap <Down> <esc><Down>
"inoremap <Left> <esc><Left>
"inoremap <Right> <esc><Right>


" autocmd:

autocmd InsertEnter,InsertChange,CursorHold,CursorHoldI,BufWritePost,CursorMoved,CursorMovedI * if &l:modified | hi StatusLine ctermbg=196 | else | hi StatusLine ctermbg=242 | endif 
                   "  La barre de status devient rouge quand une modification n'est pas enregistrée
autocmd CursorHold,CursorHoldI *.note :write | hi StatusLine ctermbg=242 
                   "  Pour les fichiers .note, l'enregistrement est automatique 
autocmd InsertEnter * hi Normal ctermbg=17
autocmd InsertLeave * hi Normal ctermbg=None
                   "  Lorsqu'on rentre dans le mode insert, l'écran devient bleu


" Nouvelle commandes:

command LibreOffice2Sql %s/\t//g|%s/,’/,'/g   
                   "  Supprime les tab et transforme les apostrophes
command DiffOrig vert new | set buftype=nofile | read ++edit # | 0d_ | diffthis | wincmd p | diffthis
                   "  permet d'afficher la différence quand un fichier est
                   "  déjà ouvert
command Stat if getbufinfo()[0].name != '' | !printf "$(wc -m %:p | awk '{print $1}') chars\n$(wc -w %:p | awk '{print $1}') words\n$(wc -l %:p | awk '{print $1}') lines\n" | endif
command Test ! echo getregtype('%')

command UrlDecode %s/%2F/\//g | %s/%26/\&/g | %s/%3F/?/g | %s/%3D/=/g | %s/%25/%/g
                   "  Decode les urls d'alphanumériques vers URL
