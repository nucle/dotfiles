#!/bin/bash

# images directory
rep="/home/$(whoami)/Wallpaper/Landscape"
uid=$(id | awk '{print $1}' | sed "s/uid=//g" | sed "s/($(whoami))//g")
export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$(echo $uid)/bus"

# Create image list from directory
liste=("${rep}/"*)

# Compute the number of images
nbre=${#liste[@]}

# Random select
selection=$((${RANDOM} % ${nbre}))

image=${liste[${selection}]}
title=`basename ${image} .jpg`
title=${title//[_]/ }
title=${title//[-]/ }

# Image loading
#gsettings set org.mate.background picture-filename ${image}
xfconf-query  -c xfce4-desktop -p /backdrop/screen0/monitoreDP-1/workspace0/last-image -s ${image}
xfconf-query  -c xfce4-desktop -p /backdrop/screen0/monitorDP-2/workspace0/last-image -s ${image}
notify-send -i ${image} "`date +\"%H:%M\"`" "${title}"

