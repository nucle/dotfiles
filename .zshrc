# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob nomatch notify correct
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/cbr/.zshrc'
autoload -U colors && colors
autoload -Uz compinit && compinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

# End of lines added by compinstall
#PS1="%{$fg[grey]%}[%T] %B%{$fg[green]%}%n@%m%{$reset_color%}:%b%{$fg[blue]%}%~%{$reset_color%}$ "
PS1="%B%{$fg[green]%}%n@%m%{$reset_color%}%B:%F{#0087d7}%~%{%f%}%b$ "

#Color commands
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias vim='lxterminal -e nvim'
alias nvim='lxterminal -e nvim'
alias neovim='lxterminal -e nvim'

alias cal='gcal -H "\e[34m:\e[0m:\e[32m:\e[0m" -q BE'

#function _set_cursor() {
#    if [[ $TMUX = '' ]]; then
#      echo -ne $1
#    else
#      echo -ne "\ePtmux;\e\e$1\e\\"
#    fi
#}
#
# Remove mode switching delay.
KEYTIMEOUT=5

#function _set_block_cursor() { _set_cursor '\e[2 q' }
#function _set_beam_cursor() { _set_cursor '\e[0 q' }
#
#function zle-keymap-select {
#  if [[ ${KEYMAP} == vicmd ]] || [[ $1 = 'block' ]]; then
#      _set_block_cursor
#  else
#      _set_beam_cursor
#  fi
#}
#zle -N zle-keymap-select
## ensure beam cursor when starting new terminal
#precmd_functions+=(_set_beam_cursor) #
## ensure insert mode and beam cursor when exiting vim
#zle-line-init() { zle -K viins; _set_beam_cursor }
#zle-line-finish() { _set_block_cursor }
#zle -N zle-line-finish
export PATH=$PATH:~/Scripts


# Load Angular CLI autocompletion.
source <(ng completion script)
